from flask import Flask, request, jsonify
from flask_restful import Resource, Api, abort
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
api = Api(app)
ma = Marshmallow(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    gender = db.Column(db.String(120), nullable=False)


class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'gender')


user_schema = UserSchema()
users_schema = UserSchema(many=True)


class HelloWorld(Resource):
    def get(self):
        return users_schema.dump(User.query.all())

    def post(self):
        fetch = request.json
        add_data = User(name=fetch['name'], gender=fetch['gender'])
        db.session.add(add_data)
        db.session.commit()
        return user_schema.dump(add_data)


class HelloMe(Resource):
    def get(self, c_id):
        filter_data = User.query.filter_by(id=c_id).first()
        return user_schema.dump(filter_data)

    def put(self, c_id):
        filter_data = User.query.filter_by(id=c_id).first()
        if filter_data.id == c_id:
            filter_data = request.json
            add_data = User(name=filter_data['name'], gender=filter_data['gender'])
            db.session.add(add_data)
            db.session.commit()
            return user_schema.dump(use, "this is after adding")
        return jsonify({"messg": "please check it out"})

    def delete(self, c_id):
        filter_data = User.query.filter_by(id=c_id).first()
        db.session.delete(filter_data)
        db.session.commit()
        return jsonify({"message": "deleted successfully "})


api.add_resource(HelloWorld, '/')
api.add_resource(HelloMe, '/<int:c_id>')

if __name__ == '__main__':
    app.run(debug=True)
